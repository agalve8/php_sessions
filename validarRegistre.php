<?php
include "mysqlconnect.php";
$name = $_REQUEST["name"];
$user = $_REQUEST["username"];
$pass = md5($_REQUEST["password"]);



$stmt = $conn->prepare('SELECT * FROM users WHERE username=?');
$stmt->bindParam(1,$user, PDO::PARAM_STR);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if($row)
{
    //Set cookie error
    setcookie('error',"Aquest usuari ja existeix!");//,  time()+1
    header('Location: registrarse.php');
}

//Validar contra mysql
$sql = "INSERT INTO Users (Username, Password) VALUES (:Username, :Password)";
$stmt = $conn->prepare($sql);

$stmt -> bindParam(':Username',$user);
$stmt -> bindParam(':Password',$pass);
//Insert
try {
    $conn->beginTransaction();
    $stmt->execute();
    $conn->commit();
}catch (Exception $e){
    $conn->rollback();
    closeConnection();
    setcookie('error',"Error processant registre: ". $e);//,  time()+1
    header('Location: login.php');
}
    closeConnection();
    //Set cookie error
    setcookie('error',"Correcte! ara fes login amb el nou usuari");//,  time()+1
    header('Location: login.php');
?>