<?php
$servername= "localhost";
$dbname = "sider";
$username = "root";
$password = "patata";

//Fem la gestió d'errors amb les intruccions try...catch(...){}
try{
  //Creem una nova connexió a la BD
  //amb new es crea una instància de la classe PDO definint el tipus de basde de dades, nom de la base de dades, usuari i password.
  $conn = new PDO("mysql:host=$servername;dbname=$dbname",$username, $password);

  // establim el mode PDO error a exception per poder
  // recuperar les excepcions
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  echo "Connected successfully";
}
catch(PDOException $error)
{
   //Si falla la connexió amb la BD es mostra l'error.
   echo "Connection failed: " . $error->getMessage();
   closeConnection();
}

function closeConnection() {
    $conn = null;
}

?>